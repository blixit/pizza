/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Blixit 
 *
 * Created on 22 février 2017, 01:00
 */
#include <iostream>
#include <ctime>
#include <cstdlib>

#include "Loader.h"
#include "Pizza.h"
#include "Config.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    srand(time(NULL));

    int m, n;
    Loader loader = Loader();

    Pizza pizza = Pizza();
    Config initialConfig = Config();
    vector<Config> listeParts;

    loader.load(argv[1],&pizza);
    //pizza.show();

    //on récupère la configuration initiale de la pizza
    initialConfig.get(pizza); 
         
    if(initialConfig.deepCut(pizza, listeParts)){
        std::cout << "La pizza est coupée !!" << std::endl;
        std::cout << "parts : " << listeParts.size() << std::endl;
        std::cout << "aire totale : " << Config::aireTotale(listeParts) << std::endl;
        
        loader.save(std::string(argv[1])+".out",listeParts);
        
        /*
        for(int i=0; i<listeParts.size();i++){
            listeParts[i].show();
        }
        */
    }
    

    //loader.save("small.in",&pizza);
    return 0;
}

