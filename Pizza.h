/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pizza.h
 * Author: Blixit 
 *
 * Created on 22 février 2017, 12:18
 */

#ifndef PIZZA_H
#define PIZZA_H

class Pizza {
public:
    Pizza();
    Pizza(const Pizza& orig);
    virtual ~Pizza();
		void setRow(int const& value){row = value;};
		int getRow() const{return row;};
		void setCol(int const& value){col = value;};
		int getCol() const{return col;};
		void setL(int const& value){L = value;};
		int getL() const{return L;};
		void setH(int const& value){H = value;};
		int getH() const{return H;};
		bool ** getCells() const{ return cells;};
		void setCells(bool ** cellules){ cells = cellules;};

		void show();
private:
		bool ** cells;
		int row;
		int col;
		int L;
		int H;

};

#endif /* PIZZA_H */

