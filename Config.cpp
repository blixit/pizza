/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Config.cpp
 * Author: Blixit 
 * 
 * Created on 22 février 2017, 12:20
 */
#include <iostream>
#include <cstdlib> //rand

#include "Config.h"

Config::Config() {
    w=h=0; //dimensions
    r=c=0;
}

Config::Config(const Config& orig) {
    w = orig.w;
    h = orig.h;
    r = orig.r;
    c = orig.c;
    dv = orig.dv;
    dh = orig.dh;
}

Config::~Config() {
}

void Config::show(){
	std::cout << "Config " << std::endl;
	std::cout << "r : " << r << std::endl;
	std::cout << "c : " << c << std::endl;
	std::cout << "w : " << w << std::endl;
	std::cout << "h : " << h << std::endl;
	std::cout << "dv : " << dv.size() << std::endl;
	std::cout << "dh : " << dh.size() << std::endl;
}

void Config::get(Pizza const& pizza){
	w = pizza.getCol();
	h = pizza.getRow();
	r = 0;
	c = 0; 
	DV();
	DH();
}

void Config::DV(){
	for (int i = 0; i < w-1; ++i){
		dv.push_back(i+1);
	}
}

void Config::DH(){
	for (int i = 0; i < h-1; ++i){
		dh.push_back(i+1);
	}
}

int Config::getAire() const{
    return w*h;
}

int Config::aireTotale(std::vector<Config> const& liste) {
    int aire = 0;
    for(int i=0; i<liste.size();i++){
        aire += liste.at(i).getAire();
    }
    return aire;
}

/**
*	On part du principe que la configuration est forcément découpable.
*	si on ne parvient pas à découper dans la direction aléatoire, on le fait dans l'autre direction
*/
bool Config::decoupe(Config* little, Config* big, int* lignealeatoire, int* colaleatoire){ 

	bool decoupe = false; 
	*lignealeatoire = *colaleatoire = -1;

	//choix de la direction de coupe
	int direction = rand()%2, iterations = 0;

	while(!decoupe && iterations <2){

		if(direction == 0 ){ //découpe verticale
			if(dv.size() > 0){ // découpe possible
				// on choisit une position
				*colaleatoire = rand() % dv.size()+1; 

				//on retire cette position de la liste des découpes possibles 
				std::vector<int>::iterator it;
				for(it = dv.begin(); it != dv.end(); it++){
					if(*it == *colaleatoire){ 
						dv.erase(it); 
                                            break;
                                        }
				}

				little->h = big->h = h;
				little->r = big->r = r;  

				if(*colaleatoire*2 < w ){ //si l'indice de la colone de coupe est < à la moitié 
					little->w = *colaleatoire;
					little->c = c;
					big->w = w-*colaleatoire;
					big->c = c + (*colaleatoire);
				}else{    
					big->w = *colaleatoire;
					big->c = c;
					little->w = w- (*colaleatoire);
					little->c = c + (*colaleatoire);
				} 
				//
				decoupe = true; 
			}else{ 
				direction = 1 - direction;
			}			
		} 
                
		else if(direction == 1 ){ //découpe horizontale
			if(dh.size() > 0){ // découpe possible
				// on choisit une position
				*lignealeatoire = rand() % dh.size()+1; 

				//on retire cette position de la liste des découpes possibles 
				std::vector<int>::iterator it;
				for(it = dh.begin(); it != dh.end(); it++){
					if(*it == *lignealeatoire){
						dh.erase(it);
                                                break;
                                        }
				}

				little->w = big->w = w;
				little->c = big->c = c; 

				if(*lignealeatoire*2 < w ){ //si l'indice de la ligne de coupe est < à la moitié
					little->h = *lignealeatoire;
					little->r = r;
					big->h = h-*lignealeatoire;
					big->r = r + (*lignealeatoire);
				}else{
					big->h = *lignealeatoire;
					big->r = r;
					little->h = h-(*lignealeatoire);
					little->r = r + (*lignealeatoire);
				} 

				decoupe = true;
			}else{
				direction = 1 - direction;
			} 
		}
		iterations++;
	}

	// si on fait plus de 2 tours, la part n'est pas divisible
	if (iterations > 2) 
		return false;
 
	little->DV();
	little->DH();
	big->DV();
	big->DH();
        
        //comparaison des aires : si l'aire de little est > à celle de big, on les inverse
        if((little->w * little->h) > (big->w * big->h)){
            Config tmp;
            tmp = *little;
            *little = *big;
            *big = tmp;
        }


	return decoupe;
}

/**
 * Vérifie si la config est valide pour une pizza donnée
 * @param pizza
 * @return 
 * 2 si la part est valide
 * 1 si partiellement valide => peut etre coupée
 * 0 non valide
 */
int Config::isValide(Pizza const& pizza){
    //calcul de l'aire
    int aire = w*h;
    
    //aire A est telle que 2*L < A <= H
    if(aire < 2*L ) //non valide
        return 0; 
    
    int nbTomates=0, nbChampi=0;
    
    bool **cells = pizza.getCells();
    for (int i = r; i < r+h; ++i){
        for (int j = c; j < c+w; ++j)	{ 
                if(cells[i][j])
                    nbTomates++; 
        } 
    }
    nbChampi = aire - nbTomates;
    
    if(nbChampi<L || nbTomates<L) //nb d'ingrédients insuffisant
        return 0;
    
    
    if(aire > H) // partiellement valide car peut être découpé
        return 1;
    
    return 2;
}

bool Config::deepCut(Pizza const& pizza, std::vector<Config>& listeParts){
    static int DEEP = 0;
    
    //if(DEEP % 100 == 0)
    //std::cout << "DEEP " << DEEP << std::endl; 
    
    //nb d'essaies possibles dans cette configuration
    int tries = 3;//(dv.size()+dh.size())*pourcentageEssais/100; 
    
    int littleValide = 0, bigValide = 0;
    
    //Tant qu'on peut couper <=> tant qu'on n'a pas essayé toutes les coupes possibles
    while(tries>0 && (dv.size() || dh.size())){
        //on découpe cette configuration en 2
        Config little, big;

        /*
         * La fonction decoupe supprime coupeH et coupeV de la liste des coupes possibles. 
         */
        int coupeH, coupeV; 
        //std::cout << DEEP << " --nb coupes V: " << dv.size()<< " nb coupes H: " << dh.size() << std::endl;
        this->decoupe(&little, &big, &coupeH, &coupeV);
        //std::cout << DEEP << " ++nb coupes V: " << dv.size()<< " nb coupes H: " << dh.size() << std::endl;
             
        
        // VALIDATION DU CONTENU DE CHAQUE PART
        
        //si une des configurations obtenues est invalide, on continue si le nombre d'essais le permet, on arrête sinon
        littleValide = little.isValide(pizza);
        bigValide = big.isValide(pizza);
        
        if(littleValide == 0 || bigValide == 0){   
            if(coupeH > -1)
                ;//std::cout << "La coupe horizontale sur l'axe " << coupeH << " est invalide" << std::endl;
            if(coupeV > -1)
                ;//std::cout << "La coupe verticale sur l'axe " << coupeV << " est invalide" << std::endl; 
            
            if((--tries) <= 0 )
                break; 
            else
                continue;
        }
        /*
        if(coupeH > -1)
            ;//std::cout << "La coupe horizontale sur l'axe " << coupeH << " est valide" << std::endl;
        if(coupeV > -1)
            ;//std::cout << "La coupe verticale sur l'axe " << coupeV << " est valide" << std::endl; 
        //std::cout << littleValide << " " << bigValide << std::endl; 
         */
        
        //si les 2 parties sont valides, on arrête
        if((littleValide+bigValide) == 4){
            listeParts.push_back(little);
            listeParts.push_back(big);
            //std::cout << "Coupe parfaite ... " << std::endl; 
            return true;
        }
        
        // VALIDATION DES COUPES FAITES DANS CHAQUE PART
        
        
        //si les parties obtenues sont partiellement valides on tente de les couper pour qu'elles soient totalement valides
        //les parties valides, on y touche pas
        
        //Version non parallèle
        std::vector<Config> listePartsLocal;
        if(littleValide==1){
            DEEP++;
            littleValide = (little.deepCut(pizza,listePartsLocal)) ? 2 : 0;
            DEEP --;
        }else{ // littleValide == 2, donc on enregistre cette part localement
            listePartsLocal.push_back(little);
        }
        if(bigValide==1){
            DEEP++;
            bigValide = (big.deepCut(pizza,listePartsLocal)) ? 2 : 0;
            DEEP --;
        }else{  // bigValide == 2, donc on enregistre cette part localement
            listePartsLocal.push_back(big);
        }
            
        //si les 2 parties (précédemment partiellement valides) sont valides, on arrête
        if((littleValide+bigValide) == 4){
            //std::cout << "Coupe imparfaite ... " << listePartsLocal.size() << std::endl; 
            for (auto elem : listePartsLocal) {
                listeParts.push_back(elem);
            }
            return true;
        }
        else
            tries--; 
            
    }
    
    
    return false;
}