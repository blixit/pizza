/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Loader.cpp
 * Author: Blixit 
 * 
 * Created on 22 février 2017, 12:15
 */
#include <iostream>
#include <cstdlib>
#include <fstream>

#include "Loader.h"

Loader::Loader() {
}

Loader::Loader(const Loader& orig) {
}

Loader::~Loader() {
}

int Loader::load(std::string const file, Pizza* pizza ){
	std::filebuf fb ;
	if(fb.open(file.c_str(),std::ios::in)){
		
		std::istream is(&fb);
		int r, c, L, H;
		is >> r;
		is >> c;
		is >> L;
		is >> H;

		pizza->setRow(r);
		pizza->setCol(c);
		pizza->setL(L);
		pizza->setH(H);

		bool ** pizzaTmp = (bool**)calloc(r,sizeof(bool*));
		for (int i = 0; i < r; ++i)	{ 
			pizzaTmp[i] =  (bool*)calloc(c,sizeof(bool));
		}

		int i=0, j=0;
		while(is && i<r){
			j = 0;
			while(is && j<c){
				char l = char(is.get());
				if(l == '\r' || l == '\n')
					continue;
				if(l == 'T')
					pizzaTmp[i][j] = 1; 
				j++;
			} 
			i++;
		}

		pizza->setCells(pizzaTmp);

		fb.close();
	}

 
	return 0;
}

int Loader::save(std::string const file, std::vector<Config> listeParts ){
    std::filebuf fb;
    if(fb.open(file.c_str(),std::ios::out)){
        
        std::ostream os(&fb);
        os << listeParts.size() << "\n";
        
        for(Config elem : listeParts){
            os << elem.r << " " << elem.c << " " << (elem.r+elem.h-1) << " " << (elem.c+elem.w-1) << "\n";
        }
        fb.close();
        return 1;
    }
    return 0;
}