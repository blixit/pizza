/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Loader.h
 * Author: Blixit 
 *
 * Created on 22 février 2017, 12:15
 */

#include <iostream>
#include <cstdlib>
#include <vector>

#include "Pizza.h"
#include "Config.h"

#ifndef LOADER_H
#define LOADER_H

class Loader {
public:
    Loader();
    Loader(const Loader& orig);
    virtual ~Loader();
    
    int load(std::string const file, Pizza* pizza );
    int save(std::string const file, std::vector<Config> listeParts ); 
private:

};

#endif /* LOADER_H */

