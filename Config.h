/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Config.h
 * Author: Blixit 
 *
 * Created on 22 février 2017, 12:20
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <vector>
#include "Pizza.h"

//Ensemble de découpages possibles.
typedef std::vector<int> DecoupeV;
typedef std::vector<int> DecoupeH;

class Config {
public:
    Config();
    Config(const Config& orig);
    virtual ~Config();
    
    static const int L = 1;
    static const int H = 6;
    static const int pourcentageEssais = 60;
    
    int w, h; //dimensions
    int r,c; //position
    DecoupeV dv; //liste des découpes verticales possibles
    DecoupeV dh; //liste des découpes horizontales possibles

    void show();

    void get(Pizza const& pizza);
    
    int getAire() const;
    
    static int aireTotale(std::vector<Config> const& liste) ;

    void DV();

    void DH();

    bool decoupe(Config* little, Config* big, int* lignealeatoire, int* colaleatoire);
    
    int isValide(Pizza const& pizza);
    
    bool deepCut(Pizza const& pizza,std::vector<Config>& listeParts);
private:

};

#endif /* CONFIG_H */

